<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

//$content['field_name']['#theme'] = "nomarkup";
//hide($content['field_name']);
if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
?>

<!-- node.tpl.php -->
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <?php print $mothership_poorthemers_helper; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
  <footer>
    <?php print $user_picture; ?>
    <span class="author"><?php print t('Written by'); ?> <?php print $name; ?></span>
    <span class="date"><?php print t('On the'); ?> <time><?php print $date; ?></time></span>

    <?php if(module_exists('comment')): ?>
      <span class="comments"><?php print $comment_count; ?> Comments</span>
    <?php endif; ?>
  </footer>
  <?php endif; ?>

  <div class="content">
    <?php print render($content['field_info_orientation']); ?>
    <div class="core-details">
      <?php print render($content['field_school_orientation']);?>
      <?php print '<div class="wrapper-field-title"><h2 class="field-label">Program name: </h2><div class="field-title">'.$title.'</div></div>'; ?>
      <?php print render($content['field_program_code_orientation']); ?>
      <?php print render($content['field_date_orientation']); ?>
      <?php print render($content['field_campus_orientation']); ?>
      <?php print render($content['field_room_orientation']); ?>

      <?php
        //formatting school name
        $fieldSchoolName = $content['field_school_orientation'][0]['#markup'];
        $schoolOne = str_replace("&amp;", "and", $fieldSchoolName);
        $schoolTwo = str_replace(",", "", $schoolOne);
        $school = urlencode($schoolTwo);
        $schoolValue = '?edit[field_school_string_orientation][und][0][value]='.$school;

        //characters to be stripped
        $badChars = array("(",")");

        //formatting program name
        $programName = urlencode(str_replace($badChars,"", $title));
        $programName = str_replace("%26amp%3B","and",$programName);
        $programNameValue = '&edit[field_program_name_orientation][und][0][value]='.$programName;

        //formatting program code
        $fieldProgramCode = $content['field_program_code_orientation'][0]['#markup'];
        $programCode = urlencode(str_replace($badChars,"", $fieldProgramCode));
        $programCodeValue = '&edit[field_program_code_orientation][und][0][value]='.$programCode;

        //formatting date
        $fieldProgramDate = $content['field_date_string_orientation'][0]['#markup'];
        $programDate = urlencode(str_replace(",","", $fieldProgramDate));
        $programDateValue = '&edit[field_date_string_orientation][und][0][value]='.$programDate;

        //formatting time
        $fieldProgramTime = $content['field_time_string_orientation'][0]['#markup'];
        $programTime = urlencode(htmlentities($fieldProgramTime));
        $programTimeValue = '&edit[field_time_string_orientation][und][0][value]='.$programTime;

        //formatting campus
        $fieldProgramCampus = $content['field_campus_orientation'][0]['#markup'];
        $programCampus = urlencode($fieldProgramCampus);
        $programCampusValue = '&edit[field_campus_string_orientation][und][0][value]='.$programCampus;

        //formatting room
        $fieldProgramRoom = $content['field_room_orientation'][0]['#markup'];
        $programRoom = urlencode(htmlentities($fieldProgramRoom));
        $programRoomValue = '&edit[field_room_orientation][und][0][value]='.$programRoom;

        //formatting address
        $fieldProgramAddress = $content['field_address_orientation'][0]['#markup'];
        $programAddress = urlencode(str_replace(",","",$fieldProgramAddress));
        $programAddressValue = '&edit[field_address_string_orientation][und][0][value]='.$programAddress;

        //formatting additional info
        $fieldInfo = field_get_items('node', $node, 'field_info_orientation');
        if ($fieldInfo) {
          $fieldProgramInfo = $content['field_info_orientation'][0]['#markup'];
          $programInfo = urlencode($fieldProgramInfo);
          $programInfoValue = '&edit[field_info_string_orientation][und][0][value]='.$programInfo;
        } else {
          $programInfoValue = "";
        }

      ?>

      <a href="/orientation/register/<?php print $schoolValue.$programNameValue.$programCodeValue.$programDateValue.$programTimeValue.$programCampusValue.$programRoomValue.$programAddressValue.$programInfoValue; ?>" class="register">Register for this Orientation</a>

    </div>
    <?php print render($content['body']); ?>
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
</article>
